import express from 'express';
import $fn from './functions';
import * as AuthController from './auth/controllers';
import * as UsersController from './users/controllers';
import * as ProjectController from './project/controllers';
import * as DataSetController from './datasets/controllers';
import * as ResourcesController from './resources/controllers';
import * as UploadController from './upload/controllers';
import * as PaymentController from './payments/controllers';
import * as LabelController from './labels/controllers';
import * as TagsController from './tags/controllers';
import {
    isAuth,
    isUploadImagesOrZipMutiple,
    isUploadAudioMutiple,
    isUploadWithoutAuthen,
} from './auth/middleware';


const router = express.Router();

//Modules Home
router.get('/', (req, res, next) => $fn.response.success(res));
//Auth Modules
router.get('/auth/me', AuthController.me);
// Upload/Download Routes
router.post('/upload', [isAuth, isUploadImagesOrZipMutiple], UploadController.uploadImageOrZip);
router.post('/user/upload/audio', [isAuth, isUploadAudioMutiple], UploadController.uploadAudio);
// User Routes
router.get('/user/:id', UsersController.getDetail);
router.post('/user/register', UsersController.register);
router.post('/user/login', UsersController.loginByPhoneOrEmail);
router.post('/user/auth/forgot', UsersController.forgotPassword);
router.post('/user/update', isAuth, UsersController.update);
// Dataset Routes
router.get('/datasets', isAuth, DataSetController.getDatasets);
router.get('/dataset/:id', isAuth, DataSetController.getDetail);
router.post('/dataset/update', isAuth, DataSetController.update);
router.post('/dataset/delete', isAuth, DataSetController.deleteDataSet);
router.post('/dataset/restore', isAuth, DataSetController.restore);
// Project Routes
router.get('/dashboard', isAuth, ProjectController.getDashboard);
router.get('/projects', isAuth, ProjectController.getList);
router.get('/project/:id', isAuth, ProjectController.getDetail);
router.post('/project/create', isAuth, ProjectController.create);
router.post('/project/update', isAuth, ProjectController.update);
router.post('/project/delete', isAuth, ProjectController.deleteProject);
router.post('/project/restore', isAuth, ProjectController.restore);
// Resources Route
router.get('/resources', isAuth, ResourcesController.getResources);
router.get('/resources/group', isAuth, ResourcesController.getCountResources);
router.post('/resource/delete', isAuth, ResourcesController.deleteResource);
router.post('/resource/restore', isAuth, ResourcesController.restore);

// Payment Route
router.post('/payment/payment-gate', isAuth, PaymentController.paymentGate);
router.post('/payment/create-invoice', isAuth, PaymentController.createInvoice);
router.get('/payment/return-url', isAuth, PaymentController.returnUrl);
router.post('/payment/payment-hook', isAuth, PaymentController.paymentHook);
router.get('/payment/export-resource', isAuth, PaymentController.exportResource);

// Label Route
router.get('/labels/count', isAuth, LabelController.count);

// Tags Route
router.get('/tags', isAuth, TagsController.get);

export default router;
