'use strict';

import configs from '../../config';
import dateFormat from 'dateformat';
import querystring from 'qs';
import sha256 from 'sha256';

const createPaymentUrl = async (_data) => {
    let tmnCode = configs.payment.vnpay.tmn_code;
    let secretKey = configs.payment.vnpay.hash_secret;
    let vnpUrl = configs.payment.vnpay.pay_url;
    let returnUrl = _data.returnUrl;
    let date = new Date();
    let createDate = dateFormat(date, 'yyyymmddHHmmss');
    let orderId = _data.invoice_data.id;
    //TODO: get rate from DB after have cronjob update rate $-vnđ
    let rate = 23000;
    let amount = _data.invoice_data.totalFee * rate;
    let bankCode = '';//optional

    let orderInfo = "Customer ID " + _data.creator + " pay invoice " + _data.invoice_data.id;
    let orderType = 130005;//phần mềm
    let locale = 'vn';
    /*let locale = req.body.language;
    if(locale === null || locale === ''){
        locale = 'vn';
    }*/
    let currCode = 'VND';
    let vnp_Params = {};
    vnp_Params['vnp_Version'] = '2';
    vnp_Params['vnp_Command'] = 'pay';
    vnp_Params['vnp_TmnCode'] = tmnCode;
    // vnp_Params['vnp_Merchant'] = ''
    vnp_Params['vnp_Locale'] = locale;
    vnp_Params['vnp_CurrCode'] = currCode;
    vnp_Params['vnp_TxnRef'] = orderId;
    vnp_Params['vnp_OrderInfo'] = orderInfo;
    vnp_Params['vnp_OrderType'] = orderType;
    vnp_Params['vnp_Amount'] = amount * 100;
    vnp_Params['vnp_ReturnUrl'] = returnUrl;
    vnp_Params['vnp_IpAddr'] = _data.ipAddr;
    vnp_Params['vnp_CreateDate'] = createDate;
    if (bankCode !== null && bankCode !== '') {
        vnp_Params['vnp_BankCode'] = bankCode;
    }

    vnp_Params = sortObject(vnp_Params);
    let signData = secretKey + querystring.stringify(vnp_Params, {encode: false});

    let secureHash = sha256(signData);

    vnp_Params['vnp_SecureHashType'] = 'SHA256';
    vnp_Params['vnp_SecureHash'] = secureHash;
    vnpUrl += '?' + querystring.stringify(vnp_Params, {encode: true});
    return vnpUrl;
    //Neu muon dung Redirect thi dong dong ben duoi
    //res.status(200).json({code: '00', data: vnpUrl})
    //Neu muon dung Redirect thi mo dong ben duoi va dong dong ben tren
    //res.redirect(vnpUrl)
}

const vnpayReturn = async (_data) => {
    return checkSum(_data);
}

const vnpayIpn = async (_data) => {
    if (checkSum(_data)) {
        return {
            responseData: {RspCode: '00', Message: 'success'},
            invoiceId: _data['vnp_TxnRef'],
            convertedFee:_data['vnp_Amount']/100,
            convertedCurrency:_data['vnp_CurrCode'],
            payDate:_data['vnp_PayDate'],
            transactionNumber:_data['vnp_TransactionNo'],
        };
    } else {
        return {
            responseData: {RspCode: '97', Message: 'Fail checksum'},
            invoiceId: false
        };
    }
}

function checkSum(_data) {
    let secureHash = _data['vnp_SecureHash'];
    delete _data['vnp_SecureHash'];
    delete _data['vnp_SecureHashType'];

    let vnp_Params = sortObject(_data);

    //let tmnCode = configs.payment.vnpay.tmn_code;
    let secretKey = configs.payment.vnpay.hash_secret;

    let signData = secretKey + querystring.stringify(vnp_Params, {encode: false});

    let checkSum = sha256(signData);

    if (secureHash === checkSum) {
        //Kiem tra xem du lieu trong db co hop le hay khong va thong bao ket qua
        //res.render('success', {code: vnp_Params['vnp_ResponseCode']})
        return true;
    } else {
        //res.render('success', {code: '97'})
        return false;
    }
}

function sortObject(o) {
    let sorted = {},
        key, a = [];

    for (key in o) {
        if (o.hasOwnProperty(key)) {
            a.push(key);
        }
    }

    a.sort();

    for (key = 0; key < a.length; key++) {
        sorted[a[key]] = o[a[key]];
    }
    return sorted;
}

module.exports = {createPaymentUrl, vnpayReturn, vnpayIpn};
