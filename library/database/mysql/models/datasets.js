'use strict';
import bcrypt from 'bcrypt';
import slugify from 'slugify';
import uuid from 'uuid/v4';

module.exports = function (sequelize, DataTypes) {
    const dataSets = sequelize.define('Datasets', {
        id: {
            type: DataTypes.UUIDV4,
            allowNull: false,
            primaryKey: true,
            defaultValue: () => uuid(),
        },
        name: {
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        slug: {
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        creator: {
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        projectId: {
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        totalTempPrice: {
            type: DataTypes.DECIMAL(16, 2),
            allowNull: true,
        },
        realPrice: {
            type: DataTypes.DECIMAL(16, 2),
            allowNull: true,
        },
        pathZip: {
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        deletedAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },
    }, {
        tableName: 'datasets',
        timestamps: true,
        paranoid: true,
        defaultScope: {
            attributes: {exclude: ['pathZip', 'deletedAt']},
        },
        scopes: {
            withPassword: {
                attributes: {},
            },
        },
        hooks: {
            beforeCreate: async (instance, options) => {
                instance.dataValues.slug = slugify(instance.dataValues.name);
            },
            beforeUpdate: async ({attributes, where}) => {
                console.log('beforeUpdate');
            },
            beforeBulkUpdate: async ({attributes, where}) => {
            },
            beforeBulkCreate: async (instance, options) => {
                // for (const user of instance) {
                //     user.dataValues.password = await bcrypt.hash(user.dataValues.password, bcrypt.genSaltSync(8));
                // }
            },
        },
    });


    dataSets.associate = models => {
        // dataSets.belongsTo(models.Users, {foreignKey: 'creator', as: 'user'});
        // dataSets.hasMany(models.Resources, {foreignKey: 'dataSetId', as:'resource'});
    };

    dataSets.prototype.toJSON = function () {
        return Object.assign({}, this.get());
    };


    return dataSets;
};
