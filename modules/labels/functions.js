'use strict';
import jwt from 'jsonwebtoken';
import Sequelize from 'sequelize';
import configs from '../../config';
import DB from '../../library/database/mysql/index';
import request from 'request-promise';

const count = async (_data) => {
    // return DB.Labels.count(
    //     {
    //         paranoid: _data.paranoid ? false : null,
    //         where: _data.deletedAt ? {deletedAt: {$not: null}} : _data.where,
    //         //group: [Sequelize.fn('date_trunc', 'day', Sequelize.col('createdAt'))]
    //     },
    // );
    return DB.Labels.findAll({
        paranoid: _data.paranoid ? false : null,
        where: _data.deletedAt ? {deletedAt: {$not: null}} : _data.where,
        group: [DB.Sequelize.fn('date_format', DB.Sequelize.col('createdAt'), `%d-%m-%Y`)],
        attributes: [
            [DB.Sequelize.fn('count', DB.Sequelize.col('id')), 'count'],
            [DB.Sequelize.fn('date_format', DB.Sequelize.col('createdAt'), '%d-%m-%Y'), 'days'],
        ],
        sort: [['createdAt', 'desc']],
    });
};

const countAll = async (_data) => {
    return DB.Labels.count({
        paranoid: _data.paranoid ? false : null,
        where: _data.deletedAt ? {deletedAt: {$not: null}} : _data.where
    },);
};



export {
    count,
    countAll
};
