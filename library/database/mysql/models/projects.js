'use strict';
import bcrypt from 'bcrypt';
import uuid from 'uuid/v4';
import slugify from 'slugify';

module.exports = function (sequelize, DataTypes) {
  const projects = sequelize.define('Projects', {
    id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true,
      defaultValue: () => uuid(),
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    slug: {
      type: DataTypes.STRING(255),
      allowNull: true,
      unique: true,
    },
    description: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    creator: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    status: {
      type: DataTypes.INTEGER(1),
      defaultValue: 1,
    },
    type: {
      type: DataTypes.INTEGER(1),
      defaultValue: 0,
    },
    personal: {
      type: DataTypes.INTEGER(1),
      defaultValue: 1,
    },
    totalResource:{
      type: DataTypes.INTEGER(),
      defaultValue: 0,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
    tableName: 'projects',
    timestamps: true,
    paranoid: true,
    scopes: {
      withPassword: {
        attributes: {},
      },
    },
    hooks: {
      beforeCreate: async (instance, options) => {
        instance.dataValues.slug = slugify(instance.dataValues.name);
      },
      beforeUpdate: async ({ attributes, where }) => {
        console.log('beforeUpdate');
      },
      beforeBulkUpdate: async ({ attributes, where }) => {
      },
      beforeBulkCreate: async (instance, options) => {
        // for (const user of instance) {
        //     user.dataValues.password = await bcrypt.hash(user.dataValues.password, bcrypt.genSaltSync(8));
        // }
      },
    },
  });


  projects.associate = models => {
    projects.belongsTo(models.Users, { foreignKey: 'creator', as: 'user' });
    projects.hasMany(models.Resources, { as: 'resource' });
    projects.hasMany(models.ProjectTag, { as: 'projectTag' });
    projects.hasMany(models.ProjectMeta, { as: 'meta' });
  };

  projects.prototype.toJSON = function () {
    return Object.assign({}, this.get());
  };


  return projects;
};
