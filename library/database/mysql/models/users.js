'use strict';
import bcrypt from 'bcrypt';
import uuid from 'uuid/v4';

module.exports = function (sequelize, DataTypes) {
    const Users = sequelize.define('Users', {
        id: {
            type: DataTypes.STRING(36),
            allowNull: false,
            primaryKey: true,
            defaultValue: () => uuid(),
        },
        username: {
            type: DataTypes.STRING(255),
            allowNull: false,
            unique: true,
            validate: {
                isUnique: function (value, next) {
                    Users.findOne({
                        where: { username: value },
                        attributes: ['id'],
                    }).then(function (user) {
                        if (user) {
                            return next('User name already exist!');
                        }
                        return next();
                    }).catch(function (err) {
                        return next(err);
                    });

                },
            },
        },
        fullName: {
            type: DataTypes.STRING(255),
            allowNull: true,
            defaultValue: null,
        },
        address: {
            type: DataTypes.STRING(255),
            allowNull: true,
            defaultValue: null,
        },
        personCardID: {
            type: DataTypes.INTEGER(255),
            allowNull: true,
            unique: true,
            defaultValue: null,
        },
        phone: {
            type: DataTypes.INTEGER(255),
            allowNull: true,
            unique: true,
            validate: {
                isUnique: function (value, next) {
                    Users.findOne({
                        where: { phone: value },
                        attributes: ['id'],
                    }).then(function (user) {
                        if (user) {
                            return next('Phone already exist!');
                        }
                        return next();
                    }).catch(function (err) {
                        return next(err);
                    });

                },
            },
            defaultValue: null,
        },
        email: {
            type: DataTypes.STRING(255),
            allowNull: false,
            unique: true,
            validate: {
                isUnique: function (value, next) {
                    Users.findOne({
                        where: { email: value },
                        attributes: ['id'],
                    }).then(function (user) {
                        if (user) {
                            return next('Email already exist!');
                        }
                        return next();
                    }).catch(function (err) {
                        return next(err);
                    });

                },
            },
        },
        password: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        type: {
            type: DataTypes.STRING(255),
        },
        thumbnail: {
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        lastLogin: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        status: {
            type: DataTypes.STRING(255),
            allowNull: true,
            defaultValue: 1,
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        isUpdated: {
            type: DataTypes.BOOLEAN(),
            allowNull: true,
            defaultValue: null,
        },
        isBusiness: {
            type: DataTypes.BOOLEAN(),
            allowNull: true,
            defaultValue: false,
        },
        deletedAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },
    }, {
        tableName: 'users',
        timestamps: true,
        defaultScope: {
            attributes: {exclude: ['password', 'deletedAt']},
        },
        scopes: {
            withPassword: {
                attributes: {},
            },
        },
        hooks: {
            beforeCreate: async (instance, options) => {
                instance.dataValues.password = await bcrypt.hash(instance.dataValues.password, bcrypt.genSaltSync(8));
            },
            beforeUpdate: async ({attributes, where}) => {
                console.log('beforeUpdate');
            },
            beforeBulkUpdate: async ({attributes, where}) => {
                if (attributes && attributes.password) attributes.password = await bcrypt.hash(attributes.password, bcrypt.genSaltSync(8));
            },
            beforeBulkCreate: async (instance, options) => {
                for (const user of instance) {
                    user.dataValues.password = await bcrypt.hash(user.dataValues.password, bcrypt.genSaltSync(8));
                }
            },
        },
        instanceMethods: {
            generateHash(password) {
                return bcrypt.hash(password, bcrypt.genSaltSync(8));
            },
            validPassword(password) {
                return bcrypt.compare(password, this.password);
            },
        },
    });

    Users.prototype.validPassword = async function (password) {
        console.log(password);
        return await bcrypt.compare(password, this.password);
    };
    // Users.associate = models => {
    //     Users.hasMany(models.UserMeta, {foreignKey: 'userId'});
    // };

    return Users;

};
