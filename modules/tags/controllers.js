import $fn from '../functions';
import configs from '../../config';
import _ from 'lodash';

/**
 * API GET LIST TAGS
 * @route POST /api/tags
 * @group Tags - Operations about Project - {"limit":10,"page":1}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const get = async (req, res, next) => {
    try {
        const filters = $fn.helpers.parseParams(req);
        let dataCount = await $fn.tags.count({
            deletedAt: filters.deletedAt,
            paranoid: filters.paranoid,
            where: filters.where
        });
        if (!filters.limit || filters.limit > 30 || filters.limit < 0) filters.limit = 10;
        if (!filters.page || filters.page > 30 || filters.page < 0) filters.page = 1;
        const [err, tags] = await $fn.helpers.wait($fn.tags.get(filters));
        if (err) return $fn.response.serverError(res, err);
        $fn.response.success(res, {total: dataCount, data: tags, limit: filters.limit});
    } catch (e) {
        return $fn.response.serverError(res, e);
    }
};


