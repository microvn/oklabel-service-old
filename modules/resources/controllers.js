import _ from 'lodash';
import $fn from '../functions';
import configs from '../../config';

/**
 * API GET LIST RESOURCES
 * @route POST /api/resources
 * @group Project - Operations about Project - {"limit":10,"page":1}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const getResources = async (req, res, next) => {
  try {
    const filters = $fn.helpers.parseParams(req);
    filters.where.creator = req.user.id;
    console.log('filters', filters);
    let dataCount = await $fn.resources.count({
      deletedAt: filters.deletedAt,
      paranoid: filters.paranoid,
      where: filters.where,
    });
    if (!filters.limit || filters.limit > 30 || filters.limit < 0) filters.limit = 10;
    if (!filters.page || filters.page > 30 || filters.page < 0) filters.page = 1;
    const [err, resource] = await $fn.helpers.wait($fn.resources.get(filters));
    console.log('e',err,filters);
    if (err) return $fn.response.serverError(res, err);
    $fn.response.success(res, {
      total: dataCount,
      data: resource,
      page: filters.page,
      limit: filters.limit,
    });
  } catch (e) {
    console.log(e);
    return $fn.response.serverError(res, e);
  }
};

/**
 * API GET LIST RESOURCES BY STATUS
 * @route POST /api/resources/group
 * @group Project - Operations about Project
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const getCountResources = async (req, res, next) => {
  try {
    const filters = $fn.helpers.parseParams(req);
    filters.where.creator = req.user.id;
    const [err, resource] = await $fn.helpers.wait($fn.resources.countResourceByStatus(req.user.id));
    if (err) return $fn.response.serverError(res, err);
    $fn.response.success(res, { total: 0, data: resource, page: 0, limit: 0 });
  } catch (e) {
    return $fn.response.serverError(res, e);
  }
};

/**
 * API DELETE RESOURCES
 * @route POST /api/resource/delete
 * @group Project - Operations about Resources - {"id":"48418f0c-7334-4f0b-90c7-b2e43cde21b3"}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const deleteResource = async (req, res, next) => {
  try {
    let _data = _.pick(req.body, [
      'id',
    ]);
    const [errResource, resourceInDoing] = await $fn.helpers.wait($fn.resources.getResourceInProcess(_data.id));
    if (errResource) return $fn.response.serverError(res, errResource);
    if (resourceInDoing !== null) return $fn.response.forbiddenError(res, configs.text.common.inProcess);
    const [err, resource] = await $fn.helpers.wait($fn.resources.deleteResource(_data.id));
    if (err) return $fn.response.serverError(res, err);
    $fn.response.success(res, resource);
  } catch (e) {
    return $fn.response.serverError(res, e);
  }
};

/**
 * API RESTORE RESOURCES
 * @route POST /api/resource/restore
 * @group Project - Operations about Project - {"id":"48418f0c-7334-4f0b-90c7-b2e43cde21b3"}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const restore = async (req, res, next) => {
  try {
    let _data = _.pick(req.body, [
      'id',
    ]);
    const [err, resource] = await $fn.helpers.wait($fn.resources.restore(_data.id));
    if (err) return $fn.response.serverError(res, err);
    $fn.response.success(res, resource);
  } catch (e) {
    return $fn.response.serverError(res, e);
  }
};
