'use strict';
import bcrypt from 'bcrypt';
import slugify from 'slugify';
import uuid from 'uuid/v4';

module.exports = function (sequelize, DataTypes) {
    const LabelTag = sequelize.define('LabelTag', {
        labelId: {
            type: DataTypes.UUIDV4,
            allowNull: false,
        },
        tagId: {
            type: DataTypes.UUIDV4,
            allowNull: false,
        },
        resourceId: {
            type: DataTypes.UUIDV4,
            allowNull: false,
        },
        isMain: {
            type: DataTypes.INTEGER(1),
            allowNull: true,
            defaultValue: 0
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        deletedAt: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        tableName: 'labelsTags',
        timestamps: true,
    });


    LabelTag.associate = models => {
        LabelTag.belongsTo(models.Labels, {foreignKey: 'labelId', as: 'labels'});
        LabelTag.belongsTo(models.Tags, {foreignKey: 'tagId', as: 'tags'});
    };

    LabelTag.prototype.toJSON = function () {
        return Object.assign({}, this.get());
    };


    return LabelTag;
};
