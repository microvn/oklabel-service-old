'use strict';
import { to } from 'await-to-js';
import crypto from 'crypto';
import moment from 'moment';
import shortId from 'short-unique-id';
import fs from 'fs';
import configs from './../../config';
// import { ExportToCsv } from "export-to-csv";

const wait = async (_promise) => {
  const [err, res] = await to(_promise);
  if (err) return [err ? err : getError(err)];
  return [null, res];
};

const genShortId = (_length = 8) => {
  let hashIds = new shortId();
  return hashIds.randomUUID(_length).toUpperCase();
};

const md5 = (_string) => crypto.createHash('md5').update(_string).digest('hex');

const isEmail = (_string) => /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(_string);

const getImages = (_string) => _string && _string.replace(configs.upload.pathImage, configs.urlCDN);

const createDir = (_path) => {
  try {
    if (!fs.existsSync(_path)) {
      return fs.mkdirSync(_path, { recursive: true });
    }
  } catch (err) {
    if (err.code !== 'EEXIST') throw err;
  }
};

const isExist = (_path) => {
  try {
    return fs.existsSync(_path);
  } catch (ex) {
    return false;
  }
};

const dateToDir = (_date) => {
  const date = _date ? moment(_date) : moment();
  try {
    return `${date.format('YYYY')}/${date.format('MM')}/${date.format('DD')}`;
  } catch (e) {
    return `${date.format('YYYY')}/${date.format('MM')}/${date.format('DD')}`;
  }
};

const getError = (_error) => {
  let stack = _error.stack ? _error.stack : '';
  let stackObject = stack.split('\n');
  let position = getPositionError(stackObject);
  let splitMessage = _error.message ? _error.message.split('\n') : [''];
  return {
    filename: position.filename,
    line: position.line,
    row: position.line,
    code: _error.code ? _error.code : null,
    message: splitMessage[splitMessage.length - 1],
    type: _error.type ? _error.type : _error.name,
    stack: stack,
    arguments: _error.arguments,
  };
};

const getPositionError = (_stack) => {
  let filename,
    line,
    row;
  try {
    let filteredStack = _stack.filter(function (s) {
      return /\(.+?\)$/.test(s);
    });
    let splitLine;
    if (filteredStack.length > 0) {
      splitLine = filteredStack[0].match(/(?:\()(.+?)(?:\))$/)[1].split(':');
    } else {
      splitLine = _stack[0].split(':');
    }
    let splitLength = splitLine.length;
    filename = splitLine[splitLength - 3];
    line = Number(splitLine[splitLength - 2]);
    row = Number(splitLine[splitLength - 1]);
  } catch (err) {
    filename = '';
    line = 0;
    row = 0;
  }
  return {
    filename: filename,
    line: line,
    row: row,
  };
};

const parseParams = (_req) => {
  let param = {
    page: 1,
    limit: 10,
  };

  let _param = _req.method === 'POST' ? _req.body : JSON.parse(_req.query.filters);
  if (_param.limit) param.limit = _param.limit && _param.limit > 100 ? 100 : _param.limit;
  if (_param.page) param.page = _param.page;
  if (_param.fields) param.fields = _param.fields;
  param.where = _param.where ? _param.where : {};
  param.deletedAt = _param.deletedAt ? _param.deletedAt : null;
  param.sort = _param.sort ? [_param.sort] : [];
  param.paranoid = _param.paranoid ? _param.paranoid : null;
  param.include = _param.include ? _param.include : null;
  return param;
};

const exportToCsv = (_original_data, _file_path, _options) => {
  try {
    let csv_option = _options;
    if (!_options) {
      csv_option = {
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalSeparator: '.',
        showLabels: false,
        showTitle: false,
        title: 'Resource Export',
        useTextFile: false,
        useBom: true,
        useKeysAsHeaders: true,
        //headers: []
      };
    }
    // const csvExporter = new ExportToCsv(csv_option);
    // let data = csvExporter.generateCsv(_original_data, true);
    // fs.appendFileSync(_file_path, data);
    return true;
  } catch (e) {
    throw e;
  }
};


const getTypeOfFile = (_type) => {
  if (!_type) return null;
  if (_type === '.png' || _type === '.jpg' || _type === '.gif' || _type === '.jpeg') {
    return 'image';
  } else if (_type === '.mp3' || _type === '.wav') {
    return 'audio';
  } else if (_type === '.txt') {
    return 'document';
  } else if (_type === '.mp4') {
    return 'video';
  } else if (_type === '.pcd') {
    return 'pcd';
  }
};

const convertTypeProject = (_type) => {
  if (_type === 'undefined' || _type === null) return null;
  switch (_type) {
    case 0:
      return 'image';
    case 1:
      return 'video';
    case 2:
      return 'pcd';
    case 3:
      return 'audio';
    case 4:
      return 'document';
  }
};

export {
  md5,
  wait,
  getImages,
  createDir,
  isEmail,
  genShortId,
  dateToDir,
  isExist,
  parseParams,
  getError,
  exportToCsv,
  getTypeOfFile,
  convertTypeProject,
};
