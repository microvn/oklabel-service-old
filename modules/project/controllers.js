import $fn from '../functions';
import configs from '../../config';
import _ from 'lodash';
import * as projectTags from '../projectTags/functions';

/**
 * API CREATE PROJECT
 * @route POST /api/project/create
 * @group Project - Operations about Project - {name:"xxxxxx","description":"xxx","dataSetId":"2e240365-99a7-4fcc-a026-3176efc3568d"}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const create = async (req, res, next) => {
  try {
    let _data = _.pick(req.body, [
      'name',
      'description',
      'type',
      'primaryTag',
    ]);
    if (!_data.name || !_data.description || _data.type === null || !_data.primaryTag) return $fn.response.clientError(res);
    _data.creator = req.user.id;
    const [err, project] = await $fn.helpers.wait($fn.projects.create(_data));
    if (err) return $fn.response.serverError(res, err);
    $fn.response.success(res, project);
  } catch (e) {
    return $fn.response.serverError(res, e);
  }
};

/**
 * API UPDATE PROJECT
 * @route POST /api/project/update
 * @group Project - Operations about Project - {"id":"xxx","name":"thang to","description":"abc xyz","dataSetId":"0cd99932-5dfa-4cd4-bb04-87d6911ddb74"}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const update = async (req, res, next) => {
  try {
    let _data = _.pick(req.body, [
      'id',
      'name',
      'description',
      'dataSetId',
      'type',
      'primaryTag',
      'personal',
      'status',
    ]);
    if (!_data.id) return $fn.response.clientError(res);
    let [errInfor, projectInfor] = await $fn.helpers.wait($fn.projects.getInfo({
      id: _data.id,
      creator: req.user.id,
    }));
    if (errInfor) return $fn.response.serverError(errInfor);
    if (projectInfor.status > 1) return $fn.response.serverError(res, configs.text.project.started);
    const [err, project] = await $fn.helpers.wait($fn.projects.update({
      id: _data.id,
      creator: req.user.id,
    }, _data));
    if (err) return $fn.response.serverError(res, err);
    const [errDataSet, dataSet] = await $fn.helpers.wait($fn.datasets.update({
      id: _data.dataSetId,
      creator: req.user.id,
    }, { projectId: _data.id }));
    if (errDataSet) return $fn.response.serverError(res, errDataSet);
    const [errPro, projectUpdated] = await $fn.helpers.wait($fn.projects.getInfo({
      id: _data.id,
      creator: req.user.id,
    }));
    if (errPro) return $fn.response.serverError(res, errPro);
    $fn.response.success(res, projectUpdated);
  } catch (e) {
    return $fn.response.serverError(res, e);
  }
};


/**
 * API GET PROJECT
 * @route POST /api/project/a7749cec-94d7-49e0-9558-1b03c7fb3501
 * @group Project - Operations about Project - {"id":"xxx"}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const getDetail = async (req, res, next) => {
  try {
    let _data = _.pick(req.params, ['id']);
    _data.creator = req.user.id;
    _data.include = true;
    let [err, project] = await $fn.helpers.wait($fn.projects.getInfo(_data));
    if (err) return $fn.response.serverError(res, err);
    let [errCount, countResource] = await $fn.helpers.wait($fn.resources.count({
      where: {
        projectId: _data.id,
        creator: req.user.id,
      },
    }));
    if (errCount) return $fn.response.serverError(res, errCount);
    let [errLabel, totalLabel] = await $fn.helpers.wait($fn.labels.countAll({ where: { projectId:  _data.id } }));
    if (errLabel) return $fn.response.serverError(res, errLabel);
    let [errTags, totalTags] = await $fn.helpers.wait($fn.projectTags.count({ where: { projectId: _data.id } }));
    if (errTags) return $fn.response.serverError(res, errTags);
    $fn.response.success(res, {
      detail: project,
      totalResource: countResource,
      totalLabel: totalLabel,
      totalTags: totalTags,
    });
  } catch (e) {
    return $fn.response.serverError(res, e);
  }
};

/**
 * API GET LIST PROJECT
 * @route POST /api/projects
 * @group Project - Operations about Project - {"limit":10,"page":1}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const getList = async (req, res, next) => {
  try {
    const filters = $fn.helpers.parseParams(req);
    filters.where.creator = req.user.id;
    console.log(filters);
    let dataCount = await $fn.projects.count({
      deletedAt: filters.deletedAt,
      paranoid: filters.paranoid,
      where: filters.where,
    });
    if (!filters.limit || filters.limit > 30 || filters.limit < 0) filters.limit = 10;
    if (!filters.page || filters.page > 30 || filters.page < 0) filters.page = 1;
    const [err, project] = await $fn.helpers.wait($fn.projects.getProjectsByUserId(filters));
    if (err) return $fn.response.serverError(res, err);
    $fn.response.success(res, { total: dataCount, data: project, limit: filters.limit });
  } catch (e) {
    return $fn.response.serverError(res, e);
  }
};

/**
 * API DELETE PROJECT
 * @route POST /api/project/delete
 * @group Project - Operations about Project - {"id":"48418f0c-7334-4f0b-90c7-b2e43cde21b3"}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const deleteProject = async (req, res, next) => {
  try {
    let _data = _.pick(req.body, [
      'id',
    ]);
    _data.creator = req.user.id;
    const [err, project] = await $fn.helpers.wait($fn.projects.deleteProject(_data));
    if (err) return $fn.response.serverError(res, err);
    $fn.response.success(res, project);
  } catch (e) {
    return $fn.response.serverError(res, e);
  }
};

/**
 * API RESTORE PROJECT
 * @route POST /api/project/delete
 * @group Project - Operations about Project - {"id":"48418f0c-7334-4f0b-90c7-b2e43cde21b3"}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const restore = async (req, res, next) => {
  try {
    let _data = _.pick(req.body, [
      'id',
    ]);
    _data.creator = req.user.id;
    const [err, project] = await $fn.helpers.wait($fn.projects.restoreProject(_data));
    if (err) return $fn.response.serverError(res, err);
    $fn.response.success(res, project);
  } catch (e) {
    return $fn.response.serverError(res, e);
  }
};

/**
 * API GET DASHBOARD
 * @route POST /api/dashboard
 * @group Project - Operations about Project - {"limit":10,"page":1}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const getDashboard = async (req, res, next) => {
  try {
    const filters = $fn.helpers.parseParams(req);
    filters.where.creator = req.user.id;
    let countDataSet = await $fn.datasets.count({ where: { creator: req.user.id } });
    let pendingProject = await $fn.projects.count({ where: { creator: req.user.id, status: 1 } });
    let doneProject = await $fn.projects.count({ where: { creator: req.user.id, status: 2 } });
    let doneResource = await $fn.resources.count({ where: { creator: req.user.id, status: 7 } });
    $fn.response.success(res, {
      countDataSet: countDataSet,
      pendingProject: pendingProject,
      doneProject: doneProject,
      doneResource: doneResource,
    });
  } catch (e) {
    return $fn.response.serverError(res, e);
  }
};
