'use strict';
import Stripe from 'stripe';
import configs from '../../config';
import logger from "../logger";
import * as helper from "../../modules/helper/helper";

const stripe = Stripe(configs.payment.stripe.secret_key);

const createCharge = async (_data) => {
    let trace_path = "createCharge in stripe payment library";
    try {
        return await stripe.charges.create({
            amount: Math.round(_data.amount),
            currency: 'usd',
            metadata: {},
            source: _data.token,
            description: _data.content,
        });
    } catch (e) {
        logger.slack(helper.getError(e), trace_path);
        return false;
    }
};

const handleError = (_error) => {
    switch (_error.type) {
        case 'StripeCardError':
            // A declined card error
            return _error.message;
        case 'RateLimitError':
            // Too many requests made to the API too quickly
            return _error.message;
        case 'StripeInvalidRequestError':
            // Invalid parameters were supplied to Stripe's API
            return _error.message;
        case 'StripeAPIError':
            // An error occurred internally with Stripe's API
            return _error.message;
        case 'StripeConnectionError':
            // Some kind of error occurred during the HTTPS communication
            return _error.message;
        case 'StripeAuthenticationError':
            // You probably used an incorrect API key
            return _error.message;
    }
};


module.exports = {handleError, createCharge};
