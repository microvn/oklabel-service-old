'use strict';
import DB from '../../library/database/mysql/index';
import randomize from 'randomatic';
import * as resource_function from "../../modules/resources/functions";
import configs from '../../config';
import * as helper from '../../modules/helper/helper';
import moment from 'moment';

const createInvoice = async (_data) => {
    //create invoice-> transaction update invoice id on all resource
    try {
        return await DB.sequelize.transaction(async (t) => {
            let invoice = await DB.Invoices.create({
                invoiceNumber: randomize('Aa0', 12),
                userId: _data.creator,
                datasetId: _data.datasetId
            }, {transaction: t});

            if (!invoice) throw new Error("Can not create invoice");

            let invoice_resource_updating = await DB.Resources.update({
                customerInvoiceId: invoice.id
            }, {
                where: {
                    creator: _data.creator,
                    datasetId: _data.datasetId,
                    status: resource_function.status_list.completed,
                    customerInvoiceId: {
                        $eq: null
                    }
                },
                transaction: t
            });

            if (!invoice_resource_updating) throw new Error("Can not update invoice ID into resources");

            let amount_result = await DB.Resources.findOne({
                where: {customerInvoiceId: invoice.id},
                attributes: [
                    [DB.sequelize.fn('sum', DB.sequelize.col('customerFee')), 'total_amount']
                ],
                raw: true,
                transaction: t
            });


            if (amount_result.total_amount <= 0) throw new Error("Can not find any resource to create invoice");

            let update_invoice_fee = await DB.Invoices.update({
                totalFee: amount_result.total_amount
            }, {
                where: {
                    id: invoice.id
                },
                transaction: t
            });

            if (!update_invoice_fee) throw new Error("Can not update invoice fee");

            return invoice;

        });
    } catch (error) {
        console.log(error);
        return false;
    }
};

const getInvoiceById = async (_id, _column = ['*']) => {
    return DB.Invoices.findOne({
        where: {id: _id},
        attributes: _column,
        raw: true,
    });
};

const renderExportFile = async (_data) => {
    try {
        _data.page = 1;
        _data.limit = 100;
        let extension = _data.extension;
        let folder_path = configs.export_resource.path + '/' + helper.dateToDir();
        let file_path = folder_path + '/' + Date.now();

        let resources = [];
        if (_data.hasOwnProperty('datasetId')) {
            file_path = folder_path + '/' + _data.dataSetId + "_" + Date.now();
            resources = await resource_function.getChargedResourceByDatasetIdAndCreator(_data);
        } else if (_data.hasOwnProperty('invoiceId')) {
            file_path = folder_path + '/' + _data.invoiceId + "_" + Date.now();
            resources = await resource_function.getChargedResourceByInvoiceId(_data);
        }
        file_path += '.' + extension;
        await helper.createDir(folder_path);

        while (_data.page === 1 || resources.length > _data.limit) {
            let prepare_data = [];
            for (let resource of resources){
                prepare_data.push({
                    ID: resource.id,
                    DatasetId: resource.dataSetId,
                    OriginalName: resource.originalName,
                    PathStorage: resource.path,
                    Label: resource.labelJson,
                    CreatedAt: resource.createdAt,
                    ExportAt: moment().format('MMMM Do YYYY, h:mm:ss a')
                });
            }
            console.log(prepare_data);
             switch (extension) {
                 case configs.export_resource.format.csv:
                     helper.exportToCsv(prepare_data,file_path);
             }
            _data.page++;
            if (_data.hasOwnProperty('datasetId')) {
                resources = await resource_function.getChargedResourceByDatasetIdAndCreator(_data);
            } else if (_data.hasOwnProperty('invoiceId')) {
                resources = await resource_function.getChargedResourceByInvoiceId(_data);
            }
        }
        return file_path;

    } catch (e) {
        console.log(e);
        return false;
    }
}


export {
    createInvoice,
    getInvoiceById,
    renderExportFile
};
