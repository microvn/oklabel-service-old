'use strict';
import bcrypt from 'bcrypt';
import uuid from 'uuid/v4';

module.exports = function (sequelize, DataTypes) {
    const invoices = sequelize.define('Invoices', {
        id: {
            type: DataTypes.UUIDV4,
            allowNull: false,
            primaryKey: true,
            defaultValue: () => uuid(),
        },
        invoiceNumber: {
            type: DataTypes.STRING(12),
            allowNull: false
        },
        userId: {
            type: DataTypes.STRING(36),
            allowNull: false
        },
        datasetId: {
            type: DataTypes.STRING(45),
            allowNull: true
        },
        totalFee: {
            type: DataTypes.DECIMAL,
            allowNull: false,
            defaultValue: '0.00'
        },
        convertedFee: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        convertedCurrency: {
            type: DataTypes.STRING(45),
            allowNull: true
        },
        payDate:{
            type: DataTypes.DATE,
            allowNull: true
        },
        transactionNumber:{
            type: DataTypes.STRING(255),
            allowNull: true
        },
        transactionData:{
            type: DataTypes.JSON,
            allowNull: true
        },
        description: {
            type: DataTypes.STRING(45),
            allowNull: true
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: false
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: false
        },
        deletedAt: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        tableName: 'invoices',
        timestamps: true,
        paranoid: true,
        scopes: {
            withPassword: {
                attributes: {},
            },
        },
        hooks: {
            beforeCreate: async (instance, options) => {
                console.log(instance.dataValues.name)
                // instance.dataValues.slug = slugify(instance.dataValues.name);
            },
            beforeUpdate: async ({attributes, where}) => {
                console.log('beforeUpdate');
            },
            beforeBulkUpdate: async ({attributes, where}) => {
            },
            beforeBulkCreate: async (instance, options) => {
                // for (const user of instance) {
                //     user.dataValues.password = await bcrypt.hash(user.dataValues.password, bcrypt.genSaltSync(8));
                // }
            },
        },
    });


    invoices.associate = models => {
        invoices.belongsTo(models.Users, {foreignKey: 'userId', as: 'user'});
    };

    invoices.prototype.toJSON = function () {
        return Object.assign({}, this.get());
    };


    return invoices;
};
