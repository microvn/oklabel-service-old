'use strict';

import * as stripe from './stripe';
import * as vnpay from './vnpay';
import configs from '../../config';


const paymentByGate = async (_data) => {
    switch(_data.gateName) {
        case configs.payment.stripe.driver:
            return await stripe.createCharge(_data);
            break;
        case configs.payment.vnpay.driver:
            return await vnpay.createPaymentUrl(_data);
            break;
        default:
            return false;
            break;
    }
};

const returnByGate = async (_data) => {
    if(_data.hasOwnProperty('vnp_SecureHash')){
        return await vnpay.vnpayReturn(_data);
    }
    return false;
};

const hookByGate = async (_data) => {
    if(_data.hasOwnProperty('vnp_SecureHash')){
        return await vnpay.vnpayIpn(_data);
    }
    return {
        responseData: {},
        invoiceId: false
    };
};

module.exports = {
    paymentByGate,
    returnByGate,
    hookByGate
}
