'use strict';
import jwt from 'jsonwebtoken';
import Sequelize from 'sequelize';
import configs from '../../config';
import DB from '../../library/database/mysql/index';
import request from 'request-promise';

const updateLoginAt = async (_userId) => {
    return await update({lastLogin: new Date()}, _userId);
};

const getInfo = (_options, _column = ['*']) => {
    return DB.Datasets.findOne({
        where: {id: _options.id, creator: _options.creator},
        attributes: _column,
        raw: true,
    });
};

const getInfoByCreator = (_creator_id, _column = ['*']) => {
    return DB.Datasets.findOne({
        where: {creator: _creator_id},
        attributes: _column,
        raw: true,
    });
};

const create = async (_data) => {
    return await DB.Datasets.create(_data);
};

const count = async (_data) => {
    return DB.Datasets.count(
        {
            paranoid: _data.paranoid ? false : null,
            where: _data.deletedAt ? {deletedAt: {$not: null}} : _data.where
        },
    );
};

const get = async (_data) => {
    const page = _data.page;
    const limit = _data.limit;
    let offset = limit * (page - 1);
    let where = {
        paranoid: _data.paranoid ? false : null,
        where: _data.deletedAt ? {deletedAt: {$not: null}} : _data.where,
        limit: limit,
        offset: offset,
        attributes: _data.fields,
        order: _data.sort,
    };
    if (_data.include) {
        where = {
            ...where,
            include: [
                {
                    model: DB.Resources,
                    require: false,
                    as: 'resource',
                }
            ]
        }
    }
    return await DB.Datasets.findAll(where);
};


const update = async (options, _data) => {
    return DB.Datasets.update(_data, {
        where: {
            id: options.id,
            creator: options.creator,
        },
        returning: true, plain: true
    });
};

const deleteDataSet = async (_options) => {
    return await DB.Datasets.destroy({
        where: {
            id: _options.id,
            creator: _options.creator
        }
    });
};

const restoreDataSet = async (_options) => {
    const project = await DB.Datasets.findOne({
        where: {id: _options.id, creator: _options.creator},
        paranoid: false
    });
    return await project.restore();
};

export {
    updateLoginAt,
    create,
    getInfo,
    getInfoByCreator,
    count,
    get,
    update,
    deleteDataSet,
    restoreDataSet
};
