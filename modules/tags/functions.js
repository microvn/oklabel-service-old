'use strict';
import jwt from 'jsonwebtoken';
import Sequelize from 'sequelize';
import configs from '../../config';
import DB from '../../library/database/mysql/index';

const count = async (_data) => {
    return DB.Tags.count(
        {
            paranoid: _data.paranoid ? false : null,
            where: _data.deletedAt ? {deletedAt: {$not: null}} : _data.where
        },
    );
};

const get = async (_data) => {
    const page = _data.page;
    const limit = _data.limit;
    let offset = limit * (page - 1);
    let where = {
        paranoid: _data.paranoid ? false : null,
        where: _data.deletedAt ? {deletedAt: {$not: null}} : _data.where,
        limit: limit,
        offset: offset,
        attributes: _data.fields,
        order: _data.sort,
    };
    return await DB.Tags.findAll(where);
};



export {
    count,
    get,
};
