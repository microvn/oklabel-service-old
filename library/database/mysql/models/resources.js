'use strict';
import bcrypt from 'bcrypt';
import uuid from 'uuid/v4';

module.exports = function (sequelize, DataTypes) {
    const resources = sequelize.define('Resources', {
        id: {
            type: DataTypes.UUIDV4,
            allowNull: false,
            primaryKey: true,
            defaultValue: () => uuid(),
        },
        projectId: {
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        type: {
            type: DataTypes.STRING(10),
            allowNull: true,
        },
        path: {
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        originalName: {
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        fileName: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        height: {
            type: DataTypes.INTEGER(255),
            allowNull: true
        },
        width: {
            type: DataTypes.INTEGER(255),
            allowNull: true
        },
        content: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        status: {
            type: DataTypes.INTEGER(255),
            allowNull: true,
            defaultValue: 1
        },
        doingBy: {
            type: DataTypes.STRING(36),
            allowNull: true
        },
        editor: {
            type: DataTypes.STRING(36),
            allowNull: true
        },
        verificator: {
            type: DataTypes.STRING(36),
            allowNull: true
        },
        intermediaryId: {
            type: DataTypes.STRING(36),
            allowNull: true
        },
        firstReceiver: {
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        lastReceiver: {
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        isError: {
            type: DataTypes.INTEGER(1),
            allowNull: true,
            defaultValue: 0
        },
        labelJson: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        labelOrigin: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        labelVerify: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        hasError: {
            type: DataTypes.INTEGER(1),
            allowNull: true
        },
        hasConflict: {
            type: DataTypes.INTEGER(1),
            allowNull: true
        },
        totalLabel: {
            type: DataTypes.INTEGER(11),
            allowNull: true
        },
        totalLabelVerifyError: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            defaultValue: 0
        },
        totalLabelConflictRight: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            defaultValue: 0
        },
        totalLabelVerifyWrong: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            defaultValue: 0
        },
        totalLabelVerifyRight: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            defaultValue: 0
        },
        totalLabelAfterConflictError: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            defaultValue: 0
        },
        labelAt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        labeledAt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        reviewAt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        reviewedAt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        verifyAt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        verifiedAt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        creator: {
            type: DataTypes.STRING(36),
            allowNull: false
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        deletedAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },
    }, {
        tableName: 'resources',
        timestamps: true,
        paranoid: true,
        scopes: {
            withPassword: {
                attributes: {},
            },
        },
        hooks: {
            beforeCreate: async (instance, options) => {
                console.log(instance.dataValues.name)
                // instance.dataValues.slug = slugify(instance.dataValues.name);
            },
            beforeUpdate: async ({attributes, where}) => {
                console.log('beforeUpdate');
            },
            beforeBulkUpdate: async ({attributes, where}) => {
            },
            beforeBulkCreate: async (instance, options) => {
                // for (const user of instance) {
                //     user.dataValues.password = await bcrypt.hash(user.dataValues.password, bcrypt.genSaltSync(8));
                // }
            },
        },
    });


    resources.associate = models => {
        resources.belongsTo(models.Projects, {foreignKey: 'projectId', as: 'project'});
    };

    resources.prototype.toJSON = function () {
        return Object.assign({}, this.get());
    };


    return resources;
};
