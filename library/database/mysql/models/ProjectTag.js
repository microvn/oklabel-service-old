'use strict';
import bcrypt from 'bcrypt';
import slugify from 'slugify';
import uuid from 'uuid/v4';

module.exports = function (sequelize, DataTypes) {
    const ProjectTag = sequelize.define('ProjectTag', {
        projectId: {
            type: DataTypes.UUIDV4,
            allowNull: false,
        },
        tagId: {
            type: DataTypes.UUIDV4,
            allowNull: false,
        },
        isMain: {
            type: DataTypes.INTEGER(1),
            allowNull: true,
            defaultValue: 0
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        deletedAt: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        tableName: 'projectTag',
        timestamps: true,
    });


    ProjectTag.associate = models => {
        ProjectTag.belongsTo(models.Projects, {foreignKey: 'projectId', as: 'project'});
        ProjectTag.belongsTo(models.Tags, {foreignKey: 'tagId', as: 'tags'});
    };

    ProjectTag.prototype.toJSON = function () {
        return Object.assign({}, this.get());
    };


    return ProjectTag;
};
