"use strict";

import extractZip from './extractZip';

module.exports = {
    extractZip: extractZip(),
};
