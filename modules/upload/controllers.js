import $fn from '../functions';
import configs from '../../config';
import _ from 'lodash';
import path from 'path';
import sizeOf from 'image-size';
import fs from 'fs';


/**
 * API UPLOAD IMAGE WITH AUTHENTICATE
 * @route POST /api/user/upload/image
 * @group Upload - Operations about Upload
 * @param {string} query.body.required - {"images":"xxxxxx","name":"xxxx","dataSetId":"48418f0c-7334-4f0b-90c7-b2e43cde21b3"}
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */
export const uploadImageOrZip = async (req, res, next) => {
    try {
        let _data = _.pick(req.body, [
            'name',
            'projectId',
        ]);
        if (!_data.name) _data.name = `DataSet ${Date.now().toString()}`;
        if (!req.user || !req.user.id) return $fn.response.authenticateError(res);
        if (!_data.projectId || req.files.length < 1 || req.fileValidationError) return $fn.response.clientError(res);
        if (req.files.length > 0 && req.files[0].mimetype === 'application/zip') {
            _data.pathZip = `${configs.upload.pathImage}/${$fn.helpers.md5(req.user.id)}/zip/${req.files[0].filename}`;
        }
        const [err, project] = await $fn.helpers.wait($fn.projects.getInfo({
            id: _data.projectId,
            creator: req.user.id,
        }));
        if (err) return $fn.response.serverError(res, err);
        if (!project) return $fn.response.notFound(res, err);

        if (project.status === 3) return $fn.response.authenticateError(res, "If Project completed, you cant upload more!");
        if (project.status === 2) return $fn.response.authenticateError(res, "If Project processing, you cant upload more!");

        console.log(path.extname(req.files[0].originalname), req.files.length,req.files[0].path);
        //Thangvt extrac zip file to images and insert
        if (req.files.length === 1 && path.extname(req.files[0].originalname) === ".zip" && configs.mimeTypeAllow.includes(req.files[0].mimetype)) {
            await $fn.helpers.wait($fn.resources.extractZipAndInsert({
                project: project,
                pathZip: req.files[0].path
            }));
            return $fn.response.success(res, {
                resources: [],
                errors: null,
                message: "Upload completed, your file zip will be extract after minutes"
            });
        } else {
            let imagesData = [];
            let errors = [];
            req.files.forEach((file) => {
                const ext = path.extname(file.originalname);
                const type = $fn.helpers.getTypeOfFile(ext, file.mimetype);
                console.log('project.type', project.type);
                console.log($fn.helpers.convertTypeProject(project.type), type);
                if ($fn.helpers.convertTypeProject(project.type) === type) {
                    if (file.mimetype !== 'application/zip') {
                        file.url = `${configs.urlCDN}/${type}/${file.filename}`;
                        file.projectId = project.id;
                        file.originalName = file.originalname;
                        file.fileName = file.filename;
                        file.type = type;
                        file.creator = req.user.id;
                    }
                    if (type === 'image') {
                        let dimensions = sizeOf(file.path);
                        file.height = dimensions.height;
                        file.width = dimensions.width;
                    }

                    if (type === 'document') {
                        file.content = fs.readFileSync(file.path, 'utf8');
                    }
                    imagesData.push(file);
                } else {
                    errors.push(`Type file: ${file.filename} not same kind project`);
                }
            });
            let [errRe, resources] = await $fn.helpers.wait($fn.resources.createMany(imagesData));
            if (errRe) return $fn.response.serverError(res, errRe);
            const [errUpdate, updateProject] = await $fn.helpers.wait($fn.projects.update({
                id: _data.projectId,
                creator: req.user.id,
            }, {
                totalResource: project.totalResource + resources.length,
            }));
            return $fn.response.success(res, {
                resources: resources,
                errors: errors,
                message: null
            });
        }

        //thangvt import images

    } catch (e) {
        console.log(e);
        return $fn.response.serverError(res, e);
    }
};

/**
 * API UPLOAD AUDIO WITH AUTHENTICATE
 * @route POST /api/user/upload/audio
 * @group Upload - Operations about Upload
 * @param {string} query.body.required - {"audio":"xxxxxx"}
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */
export const uploadAudio = async (req, res, next) => {
    try {
        if (!req.user || !req.user.id) return $fn.response.authenticateError(res);
        if (req.fileValidationError) return $fn.response.clientError(res, req.fileValidationError);
        if (!req.files) return $fn.response.clientError(res);
        let data = [];
        req.files.forEach((file) => {
            file.url = `${configs.urlCDN}${req.user.id}/audio/${file.filename}`;
            data.push(file);
        });
        return $fn.response.success(res, data);
    } catch (e) {
        console.log(e);
        return $fn.response.serverError(res, e);
    }
};
