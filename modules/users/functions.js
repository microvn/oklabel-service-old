'use strict';
import jwt from 'jsonwebtoken';
import Sequelize from 'sequelize';
import configs from '../../config';
import DB from '../../library/database/mysql/index';
import request from 'request-promise';


const getInfo = (_id, _column = ['*']) => {
    return DB.Users.findOne({
        where: {id: _id},
        attributes: _column,
        raw: true,
    });
};

const getCTVByEmail = (_email) => {
    return DB.Users.findOne({
        where: {email: _email ,type: 0 ,isBusiness : false},
        raw: true,
    });
};

const update = async (_data, _id) => {
    return await DB.Users.update(_data, {where: {id: _id}, returning: true,});
};

const create = async (_data) => {
    return await DB.Users.create(_data);
};


const generateToken = (_userId) => {
    let isUpdateLogin = updateLoginAt(_userId);
    return jwt.sign({
        id: _userId,
        type: 1,
    }, configs.jwt.key, {expiresIn: configs.jwt.expires});
};

const getUsernameOrEmail = async (_data, _column = ['*']) => {
    return await DB.Users.findOne({
        where: {
            email: _data.email
        },
    });
};
const updateLoginAt = async (_userId) => {
    return await update({lastLogin: new Date()}, _userId);
};


export {
    getInfo,
    generateToken,
    getUsernameOrEmail,
    create,
    update,
    getCTVByEmail
};
