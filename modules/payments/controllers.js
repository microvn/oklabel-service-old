import $fn from '../functions';
import configs from '../../config';
import _ from 'lodash';
import moment from 'moment';

export const paymentGate = async (req, res) => {
    try {
        const _data = _.pick(req.body, ['token', 'gateName', 'invoiceId', 'content','returnUrl']);
        if (!_data.gateName || !_data.invoiceId ) return $fn.response.clientError(res);
        _data.ipAddr = req.headers['x-forwarded-for'] ||
            req.connection.remoteAddress ||
            req.socket.remoteAddress ||
            req.connection.socket.remoteAddress;
        let invoice_data = await $fn.payments.getInvoiceById(_data.invoiceId);
        if(!invoice_data) return $fn.response.serverError(res, "Invoice is not exist!");

        _data.invoice_data = invoice_data;
        _data.creator = req.user.id;

        let payment_result = await $fn.library.payment.paymentByGate(_data);
        if (!payment_result) return $fn.response.serverError(res, "Payment procesing has error!");
        $fn.response.success(res, payment_result);
    } catch (e) {
        //console.log(e);
        return $fn.response.serverError(res, e);
    }
};

export const returnUrl = async (req, res) => {
    try {
        let params = req.query;
        let payment_result = await $fn.library.payment.returnByGate(params);
        if (!payment_result) return $fn.response.serverError(res, "Return Url has error!");
        $fn.response.success(res, payment_result);
    } catch (e) {
        console.log(e);
        return $fn.response.serverError(res, e);
    }
};

export const paymentHook = async (req, res) => {
    try {
        let params = req.query;
        let payment_result = await $fn.library.payment.hookByGate(params);
        if (payment_result.invoiceId){
            //TODO: update payment information to invoice - create new columns: convertedFee, convertedCurrency, payDate, transactionNumber, transactionData(json)

        }
        $fn.response.success(res, payment_result.responseData);
    } catch (e) {
        console.log(e);
        return $fn.response.serverError(res, e);
    }
};

export const createInvoice = async(req, res)=>
{
    try {
        let _data = _.pick(req.body, ['datasetId']);
        _data.creator = req.user.id;
        console.log(_data);
        let invoice =await $fn.payments.createInvoice(_data);
        if(invoice) return $fn.response.success(res, {invoiceId: invoice.id});
        else return $fn.response.serverError(res, "Can not create invoice!");
    } catch (e) {
        console.log(e);
        return $fn.response.serverError(res, e);
    }
}

export const exportResource = async(req, res)=>
{
    try {
        let _data = _.pick(req.body, ['datasetId','invoiceId','extension']);
        _data.creator = req.user.id;
        _data.chargedAt = moment().format('YYYY-MM-DD kk:mm:ss');
        if (_data.extension != configs.export_resource.format.csv && _data.extension != configs.export_resource.format.xml && _data.extension != configs.export_resource.format.json ) return $fn.response.serverError(res, "This file extension is not supported!");

        let resources_file =await $fn.payments.renderExportFile(_data);
        if(resources_file) return $fn.response.success(res, {url: configs.wsUrl + resources_file});
        else return $fn.response.serverError(res, "Can not get resources file!");
    } catch (e) {
        console.log(e);
        return $fn.response.serverError(res, e);
    }
}
