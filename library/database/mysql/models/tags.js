'use strict';
import bcrypt from 'bcrypt';
import uuid from 'uuid/v4';
import slugify from 'slugify';

module.exports = function (sequelize, DataTypes) {
  const Tags = sequelize.define('Tags', {
    id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true,
      defaultValue: () => uuid(),
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    slug: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    description: {
      type: DataTypes.STRING(255),
      allowNull: true,
    },
    status: {
      type: DataTypes.INTEGER(1),
      defaultValue: 1,
    },
    type: {
      type: DataTypes.INTEGER(255),
      defaultValue: null,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
    },
  }, {
    tableName: 'tags',
    timestamps: true,
    paranoid: true,
    scopes: {
      withPassword: {
        attributes: {},
      },
    },
    hooks: {
      beforeCreate: async (instance, options) => {
        instance.dataValues.slug = slugify(instance.dataValues.name, { lower: true });
      },
      beforeUpdate: async ({ attributes, where }) => {
        console.log('beforeUpdate');
      },
      beforeBulkUpdate: async ({ attributes, where }) => {
      },
      beforeBulkCreate: async (instance, options) => {
        // for (const user of instance) {
        //     user.dataValues.password = await bcrypt.hash(user.dataValues.password, bcrypt.genSaltSync(8));
        // }
      },
    },
  });


  Tags.associate = models => {
    // Tags.hasMany(models.LabelTag, {foreignKey: 'tagId'});

    Tags.belongsToMany(models.Labels, {
      through: models.LabelTag,
      as: 'labels',
      foreignKey: 'tagId',
      otherKey: 'labelId',
    });

    Tags.belongsToMany(models.Projects, {
      through: models.ProjectTag,
      as: 'projects',
      foreignKey: 'tagId',
      otherKey: 'projectId',
    });

  };

  Tags.prototype.toJSON = function () {
    return Object.assign({}, this.get());
  };


  return Tags;
};
