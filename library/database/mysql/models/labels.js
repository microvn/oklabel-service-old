'use strict';
import bcrypt from 'bcrypt';
import uuid from 'uuid/v4';
import * as Helpers from '../../../../modules/helper/helper';

module.exports = function (sequelize, DataTypes) {
    const Labels = sequelize.define('Labels', {
        id: {
            type: DataTypes.UUIDV4,
            allowNull: false,
            primaryKey: true,
            defaultValue: () => uuid(),
        },
        resourceId: {
            type: DataTypes.STRING(36),
            allowNull: false
        },
        projectId: {
            type: DataTypes.STRING(36),
            allowNull: false
        },
        editingLabels: {
            type: DataTypes.INTEGER(1),
            allowNull: true,
            defaultValue: '0'
        },
        color: {
            type: DataTypes.STRING(150),
            allowNull: true
        },
        h: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        y: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        x: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        w: {
            type: DataTypes.DECIMAL,
            allowNull: true
        },
        type: {
            type: DataTypes.STRING(10),
            allowNull: false
        },
        points: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        highlighted: {
            type: DataTypes.INTEGER(1),
            allowNull: true,
            defaultValue: 0
        },
        locked: {
            type: DataTypes.INTEGER(1),
            allowNull: true,
            defaultValue: 0
        },
        visible: {
            type: DataTypes.INTEGER(1),
            allowNull: true,
            defaultValue: 1
        },
        creator: {
            type: DataTypes.STRING(255),
            allowNull: false,
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        deletedAt: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        tableName: 'labels',
        timestamps: true,
        scopes: {},
        hooks: {
            beforeCreate: async (instance, options) => {
                console.log(instance.dataValues.name)
                // instance.dataValues.slug = slugify(instance.dataValues.name);
            },
            beforeUpdate: async ({attributes, where}) => {
                console.log('beforeUpdate');
            },
            beforeBulkUpdate: async ({attributes, where}) => {
            },
            beforeBulkCreate: async (instance, options) => {
                // for (const user of instance) {
                //     user.dataValues.password = await bcrypt.hash(user.dataValues.password, bcrypt.genSaltSync(8));
                // }
            },
        },
    });


    Labels.associate = models => {
        Labels.belongsTo(models.Resources, {foreignKey: 'resourceId'});
        // resources.belongsTo(models.Users, {foreignKey: 'editor', as: 'user'});
        // resources.belongsTo(models.Users, {foreignKey: 'firstReceiver', as: 'firstReceive'});
        // resources.belongsTo(models.Users, {foreignKey: 'lastReceiver', as: 'lastReceive'});
    };

    Labels.prototype.toJSON = function () {
        return this.get({plain: true});
    };


    return Labels;
};
