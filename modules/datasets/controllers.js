import $fn from '../functions';
import configs from '../../config';
import _ from 'lodash';
import * as datasets from "./functions";

/**
 * API GET LIST PROJECT
 * @route POST /api/datasets
 * @group Project - Operations about Project - {"limit":10,"page":1}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const getDatasets = async (req, res, next) => {
    try {
        const filters = $fn.helpers.parseParams(req);
        filters.where.creator = req.user.id;
        let dataCount = await $fn.datasets.count({
            deletedAt: filters.deletedAt,
            paranoid: filters.paranoid,
            where: filters.where
        });
        if (!filters.limit || filters.limit > 30 || filters.limit < 0) filters.limit = 10;
        if (!filters.page || filters.page > 30 || filters.page < 0) filters.page = 1;
        const [err, project] = await $fn.helpers.wait($fn.datasets.get(filters));
        if (err) return $fn.response.serverError(res, err);
        $fn.response.success(res, {total: dataCount !== null ? dataCount : 0, data: project, page: filters.page, limit: filters.limit});
    } catch (e) {
        return $fn.response.serverError(res, e);
    }
};

/**
 * API UPDATE PROJECT
 * @route POST /api/dataset/update
 * @group Project - Operations about Project - {"id":"xxx","name":"thang to"}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const update = async (req, res, next) => {
    try {
        let _data = _.pick(req.body, [
            'id',
            'name',
        ]);
        if (!_data.id) return $fn.response.clientError(res);
        const [err, project] = await $fn.helpers.wait($fn.datasets.update({
            id: _data.id,
            creator: req.user.id
        }, _data));
        if (err) return $fn.response.serverError(res, err);
        const [errDataSet, updated] = await $fn.helpers.wait($fn.datasets.getInfo({
            id: _data.id,
            creator: req.user.id
        }));
        if (errDataSet) return $fn.response.serverError(res, errDataSet);
        $fn.response.success(res, updated);
    } catch (e) {
        return $fn.response.serverError(res, e);
    }
};


/**
 * API GET PROJECT
 * @route POST /api/dataset/a7749cec-94d7-49e0-9558-1b03c7fb3501
 * @group Project - Operations about Project - {"id":"xxx"}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const getDetail = async (req, res, next) => {
    try {
        let _data = _.pick(req.params, ['id']);
        _data.creator = req.user.id;
        let [err, dataSet] = await $fn.helpers.wait($fn.datasets.getInfo(_data));
        if (err) return $fn.response.serverError(res, err);
        $fn.response.success(res, dataSet);
    } catch (e) {

        console.log(e);
        return $fn.response.serverError(res, e);
    }
};


/**
 * API DELETE PROJECT
 * @route POST /api/dataset/delete
 * @group Project - Operations about Project - {"id":"48418f0c-7334-4f0b-90c7-b2e43cde21b3"}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const deleteDataSet = async (req, res, next) => {
    try {
        let _data = _.pick(req.body, [
            'id',
        ]);
        _data.creator = req.user.id;
        const [err, project] = await $fn.helpers.wait($fn.datasets.deleteDataSet(_data));
        if (err) return $fn.response.serverError(res, err);
        $fn.response.success(res, project);
    } catch (e) {
        return $fn.response.serverError(res, e);
    }
};

/**
 * API RESTORE PROJECT
 * @route POST /api/dataset/delete
 * @group Project - Operations about Project - {"id":"48418f0c-7334-4f0b-90c7-b2e43cde21b3"}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const restore = async (req, res, next) => {
    try {
        let _data = _.pick(req.body, [
            'id',
        ]);
        _data.creator = req.user.id;
        const [err, project] = await $fn.helpers.wait($fn.datasets.restoreDataSet(_data));
        if (err) return $fn.response.serverError(res, err);
        $fn.response.success(res, project);
    } catch (e) {
        return $fn.response.serverError(res, e);
    }
};


