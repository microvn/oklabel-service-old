import $fn from '../functions';
import configs from '../../config';
import _ from 'lodash';
import * as datasets from "./functions";

/**
 * API GET LIST PROJECT
 * @route POST /api/datasets
 * @group Project - Operations about Project - {"limit":10,"page":1}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const count = async (req, res, next) => {
    try {
        const filters = $fn.helpers.parseParams(req);
        let dataCount = await $fn.labels.count({
            where: filters.where
        });
        $fn.response.success(res, dataCount);
    } catch (e) {
        return $fn.response.serverError(res, e);
    }
};



