'use strict';
import bcrypt from 'bcrypt';
import uuid from 'uuid/v4';
import * as Helpers from '../../../../modules/helper/helper';

module.exports = function (sequelize, DataTypes) {
    const projectMeta = sequelize.define('ProjectMeta', {
        id: {
            type: DataTypes.UUIDV4,
            allowNull: false,
            primaryKey: true,
            defaultValue: () => uuid(),
        },
        projectId: {
            type: DataTypes.STRING(255),
            allowNull: true,
        },
        metaName: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        metaValue: {
            type: DataTypes.STRING(),
            allowNull: true,
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        deletedAt: {
            type: DataTypes.DATE,
            allowNull: true,
        },
    }, {
        tableName: 'projectMeta',
        timestamps: true,
        scopes: {},
        hooks: {
            beforeCreate: async (instance, options) => {
                // console.log(instance.dataValues.name)
                // instance.dataValues.slug = slugify(instance.dataValues.name);
            },
            beforeUpdate: async ({attributes, where}) => {
                console.log('beforeUpdate');
            },
            beforeBulkUpdate: async ({attributes, where}) => {
            },
            beforeBulkCreate: async (instance, options) => {
                // for (const user of instance) {
                //     user.dataValues.password = await bcrypt.hash(user.dataValues.password, bcrypt.genSaltSync(8));
                // }
            },
        },
    });


    projectMeta.associate = models => {
        projectMeta.belongsTo(models.Projects, {foreignKey: 'projectId'});
        // resources.belongsTo(models.Users, {foreignKey: 'editor', as: 'user'});
        // resources.belongsTo(models.Users, {foreignKey: 'firstReceiver', as: 'firstReceive'});
        // resources.belongsTo(models.Users, {foreignKey: 'lastReceiver', as: 'lastReceive'});
    };

    projectMeta.prototype.toJSON = function () {
        return this.get({plain: true});
    };


    return projectMeta;
};
