'use strict';
import jwt from 'jsonwebtoken';
import Sequelize from 'sequelize';
import configs from '../../config';
import DB from '../../library/database/mysql/index';
import Queue from 'bull';

const status_list = {
    cancel:0,
    initial:1,//khoi tao
    hold : 2,//đã nhận
    un_hold: 3,// vừa nhả
    tagged : 4,// vừa gán nhãn
    waiting_verify : 5,//đang verify
    checking : 6,// đang hoàn thiện
    completed: 7,// hoành thành
    source_code : 8// nguồn lỗi
};

const count = async (_data) => {
    return DB.Resources.count(
        {
            paranoid: _data.paranoid ? false : null,
            where: _data.deletedAt ? {deletedAt: {$not: null}} : _data.where
        },
    );
};

const getInfo = (_id, _column = ['*']) => {
    return DB.Resources.findOne({
        where: {id: _id},
        attributes: _column,
        raw: true,
    });
};

const getResourceInProcess = (_id, _column = ['*']) => {
    return DB.Resources.findOne({
        where: {id: _id},
        attributes: _column,
        status: {$gte: 2},
        raw: true,
    });
};

const createOne = async (_data) => {
    return DB.resources.create(_data);
};

const extractZipAndInsert = async (_data) => {
    let dataQueue = new Queue('ExtractAndInsertZip', {redis: configs.queue});
    dataQueue.add(_data, {removeOnComplete: true});
    return true;
};

const createMany = async (_data) => {
    return DB.Resources.bulkCreate(_data);
};

const deleteResource = async (_id) => {
    return await DB.Resources.destroy({
        where: {
            id: _id,
        }
    });
};

const restore = async (_id) => {
    const resource = await DB.Resources.findOne({
        where: {id: _id},
        paranoid: false
    });
    return await resource.restore();
};


const get = async (_data) => {
    const page = _data.page;
    const limit = _data.limit;
    let offset = limit * (page - 1);
    let where = {
        paranoid: _data.paranoid ? false : null,
        where: _data.deletedAt ? {deletedAt: {$not: null}} : _data.where,
        limit: limit,
        offset: offset,
        attributes: _data.fields,
        order: _data.sort,
    };
    return await DB.Resources.findAll(where);
};

const countResourceByStatus = async (_data) => {
    return await DB.Resources.count({
        where: _data.where,
        group: ['status']
    });
};

const update = async (options, _data) => {
    return DB.Resources.update(_data, {
        where: {
            dataSetId: options.dataSetId,
            creator: options.creator,
        },
        returning: true, plain: true
    });
};

const getChargedResourceByDatasetIdAndCreator = async (_data, _column = ['*'])=>{
    const page = _data.page;
    const limit = _data.limit;
    let offset = limit * (page - 1);
    let options = {
        where:{
            dataSetId: _data.dataSetId,
            creator: _data.creator,
            //isCustomerCharged: true,
            //customerChargedAt:{$lte:_data.chargedAt}
            },
        limit: limit+1,
        offset: offset,
        attributes: _column,
        raw: true
    };
    return await DB.Resources.findAll(options);
}

const getChargedResourceByInvoiceId = async (_data, _column = ['*'])=>{
    const page = _data.page;
    const limit = _data.limit;
    let offset = limit * (page - 1);
    let options = {
        where:{
            customerInvoiceId: _data.invoiceId,
            creator: _data.creator,
            //isCustomerCharged: true
        },
        limit: limit+1,
        offset: offset,
        attributes: _column,
        raw: true
    };
    return await DB.Resources.findAll(options);
}

export {
    status_list,
    countResourceByStatus,
    get,
    createOne,
    getInfo,
    createMany,
    extractZipAndInsert,
    deleteResource,
    restore,
    count,
    getResourceInProcess,
    update,
    getChargedResourceByDatasetIdAndCreator,
    getChargedResourceByInvoiceId
};
