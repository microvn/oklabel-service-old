import Configs from '../../config/index';
import Bull from 'bull';
import DB from '../../library/database/mysql';
import logger from '../../library/logger';
import fs from 'fs';
import unzip from 'unzipper';
import path from 'path';
import mime from 'mime';
import * as Helpers from '../../modules/helper/helper';
import $fn from '../../modules/functions';
import sizeOf from "image-size";

module.exports = () => {

    console.log('Queue ExtractAndInsertZip Started');
    let exTractZipQueue = new Bull('ExtractAndInsertZip', {redis: Configs.queue});
    exTractZipQueue.process(function (job, done) {
        try {
            const project = job.data.project;
            const pathZip = job.data.pathZip;
            let pathCreator = `${Helpers.md5(project.creator)}`;
            let images = [];
            console.log(`Start Importing File From File Zip`, pathZip);
            if (Helpers.isExist(pathZip) || project.status !== 3 || project.status !== 2) {
                fs.createReadStream(pathZip).pipe(unzip.Parse()).on('entry', function (entry) {
                    if (entry.type === 'File' && entry.path.slice(0, 8) !== '__MACOSX') {
                        console.log(`Start Importing File: ${entry.path}`);
                        const ext = path.extname(entry.path);
                        const mimeOfFile = mime.getType(entry.path);
                        const typeOfFile = $fn.helpers.getTypeOfFile(ext);
                        const fileName = entry.path;
                        const finalName = `${Helpers.md5(fileName)}_${Date.now()}${ext}`;
                        const pathFile = `${Configs.upload.pathImage}/${pathCreator}/${typeOfFile}/${finalName}`;
                        if (Configs.extOfFileAllow.includes(ext) && Configs.mimeTypeAllow.includes(mimeOfFile)) {
                            if ($fn.helpers.convertTypeProject(project.type) === typeOfFile) {
                                entry.pipe(fs.createWriteStream(pathFile)).on('finish', () => {
                                    let _tmpFile = {
                                        projectId: project.id,
                                        path: pathFile,
                                        originalName: fileName,
                                        fileName: finalName,
                                        type: typeOfFile,
                                        creator: project.creator,
                                    }
                                    if (typeOfFile === 'image' && Helpers.isExist(pathFile)) {
                                        let dimensions = sizeOf(pathFile);
                                        _tmpFile.height = dimensions.height;
                                        _tmpFile.width = dimensions.width;
                                    }
                                    if (typeOfFile === 'document') {
                                        _tmpFile.content = fs.readFileSync(file.path, 'utf8');
                                    }
                                    images.push(_tmpFile);
                                });

                            }
                        }

                    } else {
                        entry.autodrain();
                    }
                }).on('finish', async () => {
                    console.log(`Lists file allowed Project:${project.id} - Total:${images.length}`);
                    const resources = await DB.Resources.bulkCreate(images);
                    const [err, _project] = await $fn.helpers.wait($fn.projects.getInfo({
                        id: project.id,
                        creator: project.creator,
                    }));

                    const [errUpdate, updateProject] = await $fn.helpers.wait($fn.projects.updateTotalResource({
                        id: project.id,
                    }, {
                        totalResource: _project.totalResource + resources.length,
                    }));
                    done();
                });
            }

        } catch (err) {
            console.log(err);
            logger.slack(err);
            done();
        }
    });

    exTractZipQueue.on('completed', job => {
        console.log(`Job for data id ${job.id} has been completed`);
    });

    exTractZipQueue.on('error', err => {
        logger.slack(err);
    });
};

