'use strict';
import DB from '../../library/database/mysql/index';
import _ from 'lodash';
import { json } from 'sequelize';

const updateLoginAt = async (_userId) => {
  return await update({ lastLogin: new Date() }, _userId);
};

const getInfo = async (_options, _column = ['*']) => {
  let where = {
    where: { id: _options.id, creator: _options.creator },
  };

  if (_options.include) {
    where = {
      ...where,
      include: [
        {
          model: DB.ProjectMeta,
          require: false,
          as: 'meta',
          raw: true,
        },
      ],
    };
  }
  return DB.Projects.findOne(where);
};

const getProjectsByUserId = async (_data) => {
  const page = _data.page;
  const limit = _data.limit;
  let offset = limit * (page - 1);
  return DB.Projects.findAll({
    paranoid: _data.paranoid ? false : null,
    where: _data.deletedAt ? { deletedAt: { $not: null } } : _data.where,
    limit: limit,
    offset: offset,
    attributes: _data.fields,
    order: _data.sort,
    // include: [
    //   {
    //     model: DB.Datasets,
    //     require: false,
    //     as: 'dataSet',
    //   },
    // ],
    //order: [[DB.Datasets, 'name', 'asc']]
  });
};

const create = async (_data) => {
  let project = await DB.Projects.create(_data);
  let mainTags = [];
  if (_data.primaryTag) {
    _data.primaryTag.forEach((item) => {
      mainTags.push({
        projectId: project.id,
        metaName: 'tags',
        metaValue: JSON.stringify(item),
      });
    });
  }
  await DB.ProjectMeta.bulkCreate(mainTags);
  return project;
};

const update = async (options, _data) => {
  return DB.Projects.update(_data, {
    where: {
      id: options.id,
      creator: options.creator,
    },
    returning: true, plain: true,
  }).then(() => {
    return DB.sequelize.transaction(async (t) => {
      await DB.ProjectMeta.destroy({
        where: {
          projectId: options.id,
          metaName: 'tags',
        },
        transaction: t,
      });
      let mainTags = [];
      if (_data.primaryTag.length > 0) {
        for (let primary of _data.primaryTag) {
          let _tag = await checkExistCreateTag(primary, {
            transaction: t,
          });
          if (primary.child && primary.child.length > 0) {
            for (let [indexChild, itemChild] of primary.child.entries()) {
              let _subtag = await checkExistCreateTag(itemChild, {
                transaction: t,
              });
              primary.child[indexChild].id = _subtag.id;
              primary.child[indexChild].index = indexChild;

              if (itemChild.value && itemChild.value.length > 0) {
                for (let [indexSubChild, itemSubChild] of itemChild.value.entries()) {
                  let _newItemSubChild = {
                    slug: itemSubChild.value,
                    name: itemSubChild.label,
                  };
                  let _subtag = await checkExistCreateTag(_newItemSubChild, {
                    transaction: t,
                  });
                  primary.child[indexChild].value[indexSubChild].id = _subtag.id;
                  primary.child[indexChild].value[indexSubChild].index = indexChild;
                }
              }
            }
          }
          primary.id = _tag.id;
          mainTags.push({
            projectId: options.id,
            metaName: 'tags',
            metaValue: JSON.stringify(primary),
          });
        }
      }

      return DB.ProjectMeta.bulkCreate(mainTags, { transaction: t });
    });
  });
};


const updateTotalResource = async (options, _data) => {
  return DB.Projects.update(_data, {
    where: {
      id: options.id,
    },
    returning: true, plain: true,
  });
};

const checkExistCreateTag = async (tag, _options) => {
  let _tag = await DB.Tags.findOne({
    where: {
      slug: tag.slug,
    },
  }, { transaction: _options.transaction });
  if (!_tag) {
    _tag = await DB.Tags.create({
      name: tag.name,
      slug: tag.slug,
      description: tag.name,
    }, { transaction: _options.transaction });
  }
  return _tag;
};

const deleteProject = async (_options) => {
  return await DB.Projects.destroy({
    where: {
      id: _options.id,
      creator: _options.creator,
    },
  });
};

const restoreProject = async (_options) => {
  const project = await DB.Projects.findOne({
    where: { id: _options.id, creator: _options.creator },
    paranoid: false,
  });
  return await project.restore();
};

const count = async (_data) => {
  return DB.Projects.count(
    {
      paranoid: _data.paranoid ? false : null,
      where: _data.deletedAt ? { deletedAt: { $not: null } } : _data.where,
    },
  );
};

export {
  updateLoginAt,
  update,
  updateTotalResource,
  create,
  getInfo,
  deleteProject,
  restoreProject,
  getProjectsByUserId,
  count,
};
