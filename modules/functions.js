'use strict';

import * as test from './helper/test';
import * as response from './helper/response';
import * as helper from './helper/helper';
import * as auth from './auth/functions';
import * as users from './users/functions';
import * as projects from './project/functions';
import * as datasets from './datasets/functions';
import * as resources from './resources/functions';
import * as payments from './payments/functions';
import * as labels from './labels/functions';
import * as tags from './tags/functions';
import * as projectTags from './projectTags/functions';
import * as sms from './../library/sms';
import * as mailer from './../library/mailer';
import * as payment from './../library/payment';
import * as notification from './../library/notify';


module.exports = {
    response: response,
    helpers: helper,
    test: test,
    auth: auth,
    users: users,
    projects: projects,
    datasets: datasets,
    resources: resources,
    payments: payments,
    labels: labels,
    tags: tags,
    projectTags: projectTags,
    library: {
        sms: sms,
        mailer: mailer,
        payment: payment,
        notification: notification,
    },
};
