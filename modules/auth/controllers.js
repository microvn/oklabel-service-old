import $fn from '../functions';
import _ from 'lodash';
import configs from '../../config';
import mqtt from 'mqtt';


/**
 * API GET USER FROM TOKEN
 * @route GET /api/auth/me
 * @group Auth - Operations about Authentication
 * @security JWT
 * @returns {object} 200 - An array of user info
 * @returns {Error}  default - Unexpected error
 */
export const me = async (req, res, next) => {
    const client = mqtt.connect("mqtts://m1.hongphuc.net:3112",{
        port: 3112,
        protocol: "mqtts",
        username: "B5uqSAKh1D",
        password: "amVcV57gS2",
        rejectUnauthorized: false
    })
    console.log('aaaa');

    client.on('connect', function (e) {
        console.log('Connected',e)
        client.subscribe('hello/world', function() {
            // when a message arrives, do something with it
            client.on('message', function(topic, message, packet) {
                console.log("Received '" + message + "' on '" + topic + "'");
            });
        });

        // publish a message to a topic
        client.publish('hello/world', 'my message', function() {
            console.log("Message is published");
            client.end(); // Close the connection when published
        });
    })

    client.on('error', function (e) {
        console.log('e', e)
    })

    return $fn.response.success(res, req.user);
};

