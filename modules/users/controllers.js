import $fn from '../functions';
import configs from '../../config';
import _ from 'lodash';

/**
 * API GET DETAIL USER, replace id = ObjectId; egs: 3d95db3d-8433-44c0-b0ae-c8aba00ed55d
 * @route GET /api/user/3d95db3d-8433-44c0-b0ae-c8aba00ed55d
 * @group Users - Operations about Users
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const getDetail = async (req, res, next) => {
    try {
        let _data = _.pick(req.params, ['id']);
        const [err, user] = await $fn.helpers.wait($fn.users.getInfo(_data.id));
        if (!user || !user.id) return $fn.response.notFound(res, configs.text.common.notFoundUser);
        if (err) return $fn.response.serverError(res);
        delete user.password;
        $fn.response.success(res, user);
    } catch (e) {
        return $fn.response.serverError(res, e);
    }
};

/**
 * API LOGIN BY PHONE/EMAIL && PASSWORD
 * @route POST /api/user/login
 * @group User - Operations about User
 * @param {string} param.body.required - {password:"xxxxxx","email":"xxx"}
 * @returns {object} 200 - true/false
 * @returns {Error}  default - Unexpected error
 */
export const loginByPhoneOrEmail = async (req, res, next) => {
    try {
        const _data = _.pick(req.body, ['password', 'areaCode', 'email']);
        if ((!_data.email) || !_data.password) return $fn.response.clientError(res);
        let [err, user] = await $fn.helpers.wait($fn.users.getUsernameOrEmail({
            email: _data.email
        }));
        if (err) return $fn.response.serverError(res, err);
        if (!user) return $fn.response.notFound(res, configs.text.common.notFoundUser);
        if (!user.validPassword(_data.password)) return $fn.response.notFound(res, configs.text.common.notMatchPassword);
        user = user.toJSON();
        user.token = $fn.users.generateToken(user.id);
        delete user.password;
        $fn.response.success(res, user);
    } catch (e) {
        console.log(e);
        return $fn.response.serverError(res, e);
    }
};
/**
 * API REGISTER USER
 * @route POST /api/user/register
 * @group User - Operations about User
 * @param {string} param.body.required - {"username":"xxxx","email":"email@domain.com",
 * "deviceId":"xxxx","password":"xxx","isGetToken":false}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const register = async (req, res, next) => {
    try {
        let _data = _.pick(req.body, [
            'username',
            'password',
            'email',
            'phone',
            'isGetToken',
        ]);
        if (!_data.username || !_data.email || !_data.password) return $fn.response.clientError(res);
        let [errEmail , userCTV] = await $fn.helpers.wait($fn.users.getCTVByEmail(_data.email));
        if (errEmail) return $fn.response.serverError(res, errEmail);
        if (userCTV !== null){
            let [err, user] = await $fn.helpers.wait($fn.users.update({
                isBusiness : true,
                isUpdated : true
            },userCTV.id));
            if (err) return $fn.response.serverError(res, err);
            if (Boolean(_data.isGetToken)) {
                userCTV.isBusiness = true;
                userCTV.token = $fn.users.generateToken(userCTV.id);
            }
            return $fn.response.success(res, userCTV);
        }
        _data.type = 1;
        _data.isBusiness = true;
        let [err, user] = await $fn.helpers.wait($fn.users.create(_data));
        if (err) return $fn.response.serverError(res, err);
        if (Boolean(_data.isGetToken)) {
            user = user.toJSON();
            user.token = $fn.users.generateToken(user.id);
        }
        $fn.response.success(res, user);
    } catch (e) {
        return $fn.response.serverError(res, e);
    }
};

/**
 * API UPDATE USER
 * @route POST /api/user/update
 * @group User - Operations about User
 * @param {string} param.body.required - {"username":"xxxx","email":"email@domain.com","fullName":"xxxx",
 *"password":"xxx","cmt":"0123456789","address":"xxx"}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const update = async (req, res, next) => {
    try {
        let _data = _.pick(req.body, [
            'username',
            'password',
            'fullName',
            'email',
            'phone',
            'personCardID',
            'address',
        ]);
        let [err, user] = await $fn.helpers.wait($fn.users.update(_data, req.user.id));
        if (err) return $fn.response.serverError(res, err);
        let update = await $fn.users.getInfo(req.user.id);
        delete update.password;
        $fn.response.success(res, update);
    } catch (e) {
        return $fn.response.serverError(res, e);
    }
};

/**
 * API UPDATE USER
 * @route POST /api/user/auth/forgot
 * @group User - Operations about User
 * @param {string} param.body.required - {"email":"email@domain.com"}
 * @returns {object} 200 - Object
 * @returns {Error}  default - Unexpected error
 */
export const forgotPassword = async (req, res, next) => {
    try {
        const _data = _.pick(req.body, ['email']);
        if (!_data.email || !$fn.helpers.isEmail(_data.email)) return $fn.response.clientError(res);
        let [err, user] = await $fn.helpers.wait($fn.users.getUsernameOrEmail({
            email: _data.email
        }));
        if (err) return $fn.response.serverError(res, err);
        if (!user) return $fn.response.notFound(res, configs.text.common.notFoundUser);
        $fn.response.success(res, update);
    } catch (e) {
        return $fn.response.serverError(res, e);
    }
};


